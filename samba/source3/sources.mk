
######################################################################
# object file lists
######################################################################

TDB_LIB_OBJ = lib/util_tdb.c ../lib/util/util_tdb.c \
	  lib/dbwrap.c lib/dbwrap_tdb.c \
	  lib/dbwrap_ctdb.c \
	  lib/g_lock.c \
	  lib/dbwrap_rbt.c

TDB_VALIDATE_OBJ = lib/tdb_validate.c

SMBLDAP_OBJ =  

VERSION_OBJ = lib/version.c

WBCOMMON_OBJ = ../nsswitch/wb_common.c

AFS_OBJ = lib/afs.c

AFS_SETTOKEN_OBJ = lib/afs_settoken.c

AVAHI_OBJ = 

SERVER_MUTEX_OBJ = lib/server_mutex.c

PASSCHANGE_OBJ = libsmb/passchange.c

LIBNDR_DRSUAPI_OBJ = ../librpc/ndr/ndr_drsuapi.c \
		     ../librpc/ndr/ndr_compression.c \
		     ../librpc/gen_ndr/ndr_drsuapi.c \
		     ../librpc/ndr/ndr_drsblobs.c \
		     ../librpc/gen_ndr/ndr_drsblobs.c

ZLIB_OBJ = 

COMPRESSION_OBJ = ../lib/compression/mszip.c \
				  ../lib/compression/lzxpress.c \
				  $(ZLIB_OBJ)

DRSUAPI_OBJ = $(LIBNDR_DRSUAPI_OBJ) \
	      $(COMPRESSION_OBJ)

LIBNDR_NTLMSSP_OBJ = ../librpc/gen_ndr/ndr_ntlmssp.c \
		     ../librpc/ndr/ndr_ntlmssp.c

LIBNDR_OBJ = ../librpc/ndr/ndr_basic.c \
	     ../librpc/ndr/ndr.c \
	     ../librpc/ndr/ndr_misc.c \
	     ../librpc/gen_ndr/ndr_misc.c \
	     ../librpc/gen_ndr/ndr_security.c \
	     ../librpc/ndr/ndr_sec_helper.c \
	     librpc/ndr/ndr_string.c \
	     ../librpc/ndr/uuid.c \
	     librpc/ndr/util.c \
	     ../librpc/gen_ndr/ndr_dcerpc.c

RPCCLIENT_NDR_OBJ = rpc_client/ndr.c

LIBNDR_GEN_OBJ0 = ../librpc/gen_ndr/ndr_samr.c \
		  ../librpc/gen_ndr/ndr_lsa.c

LIBNDR_GEN_OBJ1 = ../librpc/gen_ndr/ndr_netlogon.c \
		  ../librpc/ndr/ndr_netlogon.c

LIBNDR_GEN_OBJ2 = ../librpc/gen_ndr/ndr_spoolss.c \
		  ../librpc/ndr/ndr_spoolss_buf.c

LIBNDR_GEN_OBJ = ../librpc/gen_ndr/ndr_wkssvc.c \
		 $(LIBNDR_GEN_OBJ0) \
		 ../librpc/gen_ndr/ndr_dfs.c \
		 ../librpc/gen_ndr/ndr_echo.c \
		 ../librpc/gen_ndr/ndr_winreg.c \
		 ../librpc/gen_ndr/ndr_initshutdown.c \
		 ../librpc/gen_ndr/ndr_srvsvc.c \
		 ../librpc/gen_ndr/ndr_eventlog.c \
		 $(LIBNDR_GEN_OBJ1) \
		 ../librpc/gen_ndr/ndr_dssetup.c \
		 librpc/gen_ndr/ndr_notify.c \
		 ../librpc/gen_ndr/ndr_xattr.c \
		 ../librpc/ndr/ndr_xattr.c \
		 ../librpc/gen_ndr/ndr_epmapper.c \
		 ../librpc/gen_ndr/ndr_named_pipe_auth.c \
		 ../librpc/gen_ndr/ndr_ntsvcs.c \
		 $(LIBNDR_GEN_OBJ2)

RPC_PARSE_OBJ0 = rpc_parse/parse_prs.c rpc_parse/parse_misc.c

# this includes only the low level parse code, not stuff
# that requires knowledge of security contexts
RPC_PARSE_OBJ1 = $(RPC_PARSE_OBJ0)

RPC_PARSE_OBJ2 = rpc_parse/parse_rpc.c \
		 rpc_client/init_netlogon.c \
		 rpc_client/init_lsa.c

LIBREPLACE_OBJ = ./../lib/replace/replace.c ./../lib/replace/snprintf.c ./../lib/replace/getpass.c ./../lib/replace/strptime.c ./../lib/replace/timegm.c ./../lib/replace/crypt.c  ./../lib/replace/getifaddrs.c

SOCKET_WRAPPER_OBJ = 
NSS_WRAPPER_OBJ = 

LIBSAMBAUTIL_OBJ = \
		$(LIBREPLACE_OBJ) \
		$(SOCKET_WRAPPER_OBJ) \
		$(NSS_WRAPPER_OBJ)

UTIL_OBJ = ../lib/util/rbtree.c ../lib/util/signal.c ../lib/util/time.c \
		   ../lib/util/xfile.c ../lib/util/util_strlist.c  \
		   ../lib/util/util_file.c ../lib/util/data_blob.c \
		   ../lib/util/util.c ../lib/util/fsusage.c \
		   ../lib/util/params.c ../lib/util/talloc_stack.c \
		   ../lib/util/genrand.c ../lib/util/util_net.c \
		   ../lib/util/become_daemon.c ../lib/util/system.c \
		   ../lib/util/tevent_unix.c ../lib/util/tevent_ntstatus.c \
		   ../lib/util/smb_threads.c ../lib/util/util_id.c \
		   ../lib/util/blocking.c ../lib/util/rfc1738.c 

CRYPTO_OBJ = ../lib/crypto/crc32.c ../lib/crypto/md5.c \
			 ../lib/crypto/hmacmd5.c ../lib/crypto/arcfour.c \
			 ../lib/crypto/md4.c \
			 ../lib/crypto/sha256.c ../lib/crypto/hmacsha256.c \
			 ../lib/crypto/aes.c ../lib/crypto/rijndael-alg-fst.c

LIB_OBJ = $(LIBSAMBAUTIL_OBJ) $(UTIL_OBJ) $(CRYPTO_OBJ) \
	  lib/messages.c librpc/gen_ndr/ndr_messaging.c lib/messages_local.c \
	  lib/messages_ctdbd.c lib/packet.c lib/ctdbd_conn.c \
	  lib/interfaces.c lib/memcache.c \
	  lib/talloc_dict.c \
	  lib/util_transfer_file.c ../lib/async_req/async_sock.c \
	  $(TDB_LIB_OBJ) \
	  $(VERSION_OBJ) lib/charcnv.c lib/debug.c lib/fault.c \
	  lib/interface.c lib/pidfile.c \
	  lib/system.c lib/sendfile.c lib/recvfile.c lib/time.c \
	  lib/username.c \
	  ../libds/common/flag_mapping.c \
	  lib/util_pw.c lib/access.c lib/smbrun.c \
	  lib/bitmap.c lib/dprintf.c $(UTIL_REG_OBJ) \
	  lib/wins_srv.c \
	  lib/util_str.c lib/clobber.c lib/util_sid.c lib/util_uuid.c \
	  lib/util_unistr.c lib/util_file.c \
	  lib/util.c lib/jenkins_hash.c lib/util_sock.c lib/sock_exec.c lib/util_sec.c \
	  lib/substitute.c lib/dbwrap_util.c \
	  lib/ms_fnmatch.c lib/select.c lib/errmap_unix.c \
	  lib/tallocmsg.c lib/dmallocmsg.c \
	  libsmb/clisigning.c libsmb/smb_signing.c \
	  lib/iconv.c lib/pam_errors.c intl/lang_tdb.c \
	  lib/conn_tdb.c lib/adt_tree.c lib/gencache.c \
	  lib/module.c lib/events.c  ./../lib/tevent/tevent.c ./../lib/tevent/tevent_debug.c ./../lib/tevent/tevent_util.c ./../lib/tevent/tevent_fd.c ./../lib/tevent/tevent_timed.c ./../lib/tevent/tevent_immediate.c ./../lib/tevent/tevent_signal.c ./../lib/tevent/tevent_req.c ./../lib/tevent/tevent_wakeup.c ./../lib/tevent/tevent_queue.c ./../lib/tevent/tevent_standard.c ./../lib/tevent/tevent_select.c ./../lib/tevent/tevent_epoll.c \
	  lib/ldap_escape.c  modules/CP850.c modules/CP437.c \
	  lib/secdesc.c lib/util_seaccess.c ../libcli/security/secace.c \
	  ../libcli/security/secacl.c  \
	  lib/fncall.c \
	  libads/krb5_errs.c lib/system_smbd.c lib/audit.c $(LIBNDR_OBJ) \
	  lib/file_id.c lib/idmap_cache.c \
	  ../libcli/security/dom_sid.c ../libcli/security/security_descriptor.c

LIB_DUMMY_OBJ = lib/dummysmbd.c lib/dummyroot.c
LIB_NONSMBD_OBJ = $(LIB_OBJ) $(LIB_DUMMY_OBJ)

READLINE_OBJ = lib/readline.c

# Also depends on  $(SECRETS_OBJ) $(LIBSAMBA_OBJ)
# Be sure to include them into your application
POPT_LIB_OBJ = lib/popt_common.c

PARAM_WITHOUT_REG_OBJ = dynconfig.c param/loadparm.c param/util.c lib/sharesec.c lib/ldap_debug_handler.c
PARAM_REG_ADD_OBJ = $(REG_SMBCONF_OBJ) $(LIBSMBCONF_OBJ) $(PRIVILEGES_BASIC_OBJ)
PARAM_OBJ = $(PARAM_WITHOUT_REG_OBJ) $(PARAM_REG_ADD_OBJ)

KRBCLIENT_OBJ = libads/kerberos.c libads/ads_status.c

LIBADDNS_OBJ0 = libaddns/dnsrecord.c libaddns/dnsutils.c  libaddns/dnssock.c \
	       libaddns/dnsgss.c libaddns/dnsmarshall.c
LIBADDNS_OBJ = $(LIBADDNS_OBJ0) $(SOCKET_WRAPPER_OBJ)

GPEXT_OBJ = ../libgpo/gpext/gpext.c 

LIBGPO_OBJ0 = ../libgpo/gpo_ldap.c ../libgpo/gpo_ini.c ../libgpo/gpo_util.c \
	      ../libgpo/gpo_fetch.c libgpo/gpo_filesync.c ../libgpo/gpo_sec.c \
	      libgpo/gpo_reg.c \
	      $(GPEXT_OBJ)
LIBGPO_OBJ = $(LIBGPO_OBJ0)

LIBADS_OBJ = libads/ldap.c libads/ldap_printer.c \
	     libads/sasl.c libads/sasl_wrapping.c \
	     libads/krb5_setpw.c libads/ldap_user.c \
	     libads/ads_struct.c libads/kerberos_keytab.c \
             libads/disp_sec.c libads/ads_utils.c libads/ldap_utils.c \
	     libads/ldap_schema.c libads/util.c libads/ndr.c

LIBADS_SERVER_OBJ = libads/kerberos_verify.c libads/authdata.c \
		    ../librpc/ndr/ndr_krb5pac.c \
		    ../librpc/gen_ndr/ndr_krb5pac.c

SECRETS_OBJ = passdb/secrets.c passdb/machine_sid.c \
	      librpc/gen_ndr/ndr_secrets.c

LIBNBT_OBJ = ../libcli/nbt/nbtname.c \
	     ../libcli/netlogon.c \
	     ../libcli/ndr_netlogon.c \
	     ../librpc/gen_ndr/ndr_nbt.c \
	     ../librpc/gen_ndr/ndr_svcctl.c \
	     ../librpc/ndr/ndr_svcctl.c

LIBNMB_OBJ = libsmb/unexpected.c libsmb/namecache.c libsmb/nmblib.c \
	     libsmb/namequery.c ../libcli/nbt/lmhosts.c libsmb/conncache.c libads/dns.c

NTERR_OBJ = libsmb/nterr.c libsmb/smberr.c
DOSERR_OBJ = ../libcli/util/doserr.c
ERRORMAP_OBJ = libsmb/errormap.c
DCE_RPC_ERR_OBJ = ../librpc/rpc/dcerpc_error.c

LIBSMB_ERR_OBJ0 = $(NTERR_OBJ) $(DOSERR_OBJ) $(ERRORMAP_OBJ) $(DCE_RPC_ERR_OBJ)

LIBSMB_ERR_OBJ1 = ../libcli/auth/smbdes.c ../libcli/auth/smbencrypt.c ../libcli/auth/msrpc_parse.c ../libcli/auth/session.c

LIBSMB_ERR_OBJ = $(LIBSMB_ERR_OBJ0) $(LIBSMB_ERR_OBJ1) \
		 $(RPC_PARSE_OBJ1) \
		 $(SECRETS_OBJ)

LIBSMB_OBJ0 = \
	       ../libcli/auth/ntlm_check.c \
	       libsmb/ntlmssp.c \
	       libsmb/ntlmssp_sign.c \
	       $(LIBNDR_NTLMSSP_OBJ) \
	       libsmb/ntlmssp_ndr.c

LIBSAMBA_OBJ = $(LIBSMB_OBJ0) \
	       $(LIBSMB_ERR_OBJ)

LIBCLI_LDAP_MESSAGE_OBJ = ../libcli/ldap/ldap_message.c
LIBCLI_LDAP_NDR_OBJ = ../libcli/ldap/ldap_ndr.c

LIBTSOCKET_OBJ = ../lib/tsocket/tsocket.c \
		../lib/tsocket/tsocket_helpers.c \
		../lib/tsocket/tsocket_bsd.c

CLDAP_OBJ = libads/cldap.c \
	../libcli/cldap/cldap.c \
	../lib/util/idtree.c \
	$(LIBCLI_LDAP_MESSAGE_OBJ) $(LIBCLI_LDAP_NDR_OBJ) $(LIBTSOCKET_OBJ)

TLDAP_OBJ = lib/tldap.c lib/tldap_util.c lib/util_tsock.c

SCHANNEL_OBJ = ../libcli/auth/credentials.c \
	       ../libcli/auth/schannel_sign.c \
	       ../libcli/auth/schannel_state_tdb.c \
	       ../librpc/gen_ndr/ndr_schannel.c \
	       ../librpc/ndr/ndr_schannel.c \
	       passdb/secrets_schannel.c

LIBSMB_OBJ = libsmb/clientgen.c libsmb/cliconnect.c libsmb/clifile.c \
	     libsmb/clikrb5.c libsmb/clispnego.c \
	     ../libcli/auth/spnego_parse.c \
	     ../lib/util/asn1.c \
	     libsmb/clirap.c libsmb/clierror.c libsmb/climessage.c \
	     libsmb/clireadwrite.c libsmb/clilist.c libsmb/cliprint.c \
	     libsmb/clitrans.c libsmb/clisecdesc.c libsmb/clidgram.c \
	     libsmb/clistr.c libsmb/cliquota.c libsmb/clifsinfo.c libsmb/clidfs.c \
	     libsmb/clioplock.c libsmb/clirap2.c \
	     libsmb/smb_seal.c libsmb/async_smb.c \
	     libsmb/smbsock_connect.c \
	     $(LIBSAMBA_OBJ) \
	     $(LIBNMB_OBJ) \
	     $(LIBNBT_OBJ) \
	     $(CLDAP_OBJ) \
	     $(DRSUAPI_OBJ)

RPC_CLIENT_OBJ1 = rpc_client/cli_netlogon.c \
		  $(SCHANNEL_OBJ)

LIBMSRPC_OBJ = rpc_client/cli_lsarpc.c rpc_client/cli_samr.c \
	       $(RPC_CLIENT_OBJ1) \
	       $(RPC_CLIENT_OBJ) \
	       rpc_client/cli_spoolss.c \
	       rpc_client/init_spoolss.c \
	       rpc_client/init_samr.c \
		   librpc/rpc/dcerpc.c \
		   ../librpc/rpc/binding.c

LIBMSRPC_GEN_OBJ = ../librpc/gen_ndr/cli_lsa.c \
		   ../librpc/gen_ndr/cli_dfs.c \
		   ../librpc/gen_ndr/cli_echo.c \
		   ../librpc/gen_ndr/cli_srvsvc.c \
		   ../librpc/gen_ndr/cli_svcctl.c \
		   ../librpc/gen_ndr/cli_winreg.c \
		   ../librpc/gen_ndr/cli_initshutdown.c \
		   ../librpc/gen_ndr/cli_eventlog.c \
		   ../librpc/gen_ndr/cli_wkssvc.c \
		   ../librpc/gen_ndr/cli_netlogon.c \
		   ../librpc/gen_ndr/cli_samr.c \
		   ../librpc/gen_ndr/cli_dssetup.c \
		   ../librpc/gen_ndr/cli_ntsvcs.c \
		   ../librpc/gen_ndr/cli_epmapper.c \
		   ../librpc/gen_ndr/cli_drsuapi.c \
		   ../librpc/gen_ndr/cli_spoolss.c \
		   $(LIBNDR_GEN_OBJ) \
		   $(RPCCLIENT_NDR_OBJ)

#
# registry-related objects
#
UTIL_REG_OBJ = lib/util_reg.c
UTIL_REG_API_OBJ = lib/util_reg_api.c

REG_INIT_BASIC_OBJ = registry/reg_init_basic.c
REG_INIT_SMBCONF_OBJ = registry/reg_init_smbconf.c
REG_INIT_FULL_OBJ = registry/reg_init_full.c

REGFIO_OBJ = registry/regfio.c

REGOBJS_OBJ = registry/reg_objects.c

REG_BACKENDS_BASE_OBJ = registry/reg_backend_db.c

REG_BACKENDS_SMBCONF_OBJ = registry/reg_backend_smbconf.c

REG_BACKENDS_EXTRA_OBJ = registry/reg_backend_printing.c \
			 registry/reg_backend_shares.c \
			 registry/reg_backend_netlogon_params.c \
			 registry/reg_backend_prod_options.c \
			 registry/reg_backend_tcpip_params.c \
			 registry/reg_backend_hkpt_params.c \
			 registry/reg_backend_current_version.c \
			 registry/reg_backend_perflib.c

REG_BASE_OBJ = registry/reg_api.c \
	       registry/reg_dispatcher.c \
	       registry/reg_cachehook.c \
	       $(REGFIO_OBJ) \
	       $(REGOBJS_OBJ) \
	       registry/reg_util.c \
	       $(UTIL_REG_API_OBJ) \
	       lib/util_nttoken.c \
	       $(REG_BACKENDS_BASE_OBJ) \
	       $(REG_INIT_BASIC_OBJ)

REG_SMBCONF_OBJ = $(REG_BASE_OBJ) \
		  $(REG_BACKENDS_SMBCONF_OBJ) \
		  $(REG_INIT_SMBCONF_OBJ)

REG_FULL_OBJ = $(REG_SMBCONF_OBJ) \
	       $(REG_BACKENDS_EXTRA_OBJ) \
	       $(REG_INIT_FULL_OBJ) \
	       registry/reg_eventlog.c \
	       registry/reg_perfcount.c \
	       librpc/gen_ndr/ndr_perfcount.c \
	       registry/reg_util_legacy.c

LIB_EVENTLOG_OBJ = lib/eventlog/eventlog.c

RPC_LSA_OBJ = rpc_server/srv_lsa_nt.c ../librpc/gen_ndr/srv_lsa.c

RPC_NETLOG_OBJ = rpc_server/srv_netlog_nt.c \
		 ../librpc/gen_ndr/srv_netlogon.c

RPC_SAMR_OBJ = rpc_server/srv_samr_nt.c \
               rpc_server/srv_samr_util.c \
	       ../librpc/gen_ndr/srv_samr.c

RPC_INITSHUTDOWN_OBJ =  ../librpc/gen_ndr/srv_initshutdown.c rpc_server/srv_initshutdown_nt.c

RPC_REG_OBJ =  rpc_server/srv_winreg_nt.c \
	       ../librpc/gen_ndr/srv_winreg.c

RPC_DSSETUP_OBJ =  rpc_server/srv_dssetup_nt.c ../librpc/gen_ndr/srv_dssetup.c

RPC_SVC_OBJ = rpc_server/srv_srvsvc_nt.c \
	      ../librpc/gen_ndr/srv_srvsvc.c

RPC_WKS_OBJ =  ../librpc/gen_ndr/srv_wkssvc.c rpc_server/srv_wkssvc_nt.c

RPC_SVCCTL_OBJ =  rpc_server/srv_svcctl_nt.c \
		  ../librpc/gen_ndr/srv_svcctl.c \
                  services/svc_spoolss.c services/svc_rcinit.c services/services_db.c \
                  services/svc_netlogon.c services/svc_winreg.c \
                  services/svc_wins.c

RPC_NTSVCS_OBJ = rpc_server/srv_ntsvcs_nt.c \
		 ../librpc/gen_ndr/srv_ntsvcs.c

RPC_DFS_OBJ =  ../librpc/gen_ndr/srv_dfs.c rpc_server/srv_dfs_nt.c

RPC_SPOOLSS_OBJ = rpc_server/srv_spoolss_nt.c \
		  ../librpc/gen_ndr/srv_spoolss.c

RPC_EVENTLOG_OBJ = rpc_server/srv_eventlog_nt.c \
		   $(LIB_EVENTLOG_OBJ) ../librpc/gen_ndr/srv_eventlog.c

RPC_PIPE_OBJ = rpc_server/srv_pipe_hnd.c \
               rpc_server/srv_pipe.c rpc_server/srv_lsa_hnd.c

RPC_ECHO_OBJ = rpc_server/srv_echo_nt.c ../librpc/gen_ndr/srv_echo.c

RPC_SERVER_OBJ =  $(RPC_LSA_OBJ) $(RPC_REG_OBJ) $(RPC_INITSHUTDOWN_OBJ) $(RPC_DSSETUP_OBJ) $(RPC_WKS_OBJ) $(RPC_SVCCTL_OBJ) $(RPC_NTSVCS_OBJ) $(RPC_NETLOG_OBJ) $(RPC_DFS_OBJ) $(RPC_SVC_OBJ) $(RPC_SPOOLSS_OBJ) $(RPC_EVENTLOG_OBJ) $(RPC_SAMR_OBJ) $(RPC_PIPE_OBJ)

RPC_PARSE_OBJ = $(RPC_PARSE_OBJ2)

RPC_CLIENT_OBJ = rpc_client/cli_pipe.c rpc_client/rpc_transport_np.c \
	rpc_client/rpc_transport_sock.c rpc_client/rpc_transport_smbd.c

LOCKING_OBJ = locking/locking.c locking/brlock.c locking/posix.c

PRIVILEGES_BASIC_OBJ = lib/privileges_basic.c

PRIVILEGES_OBJ = lib/privileges.c

PASSDB_GET_SET_OBJ = passdb/pdb_get_set.c

PASSDB_OBJ = $(PASSDB_GET_SET_OBJ) passdb/passdb.c passdb/pdb_interface.c \
		passdb/util_wellknown.c passdb/util_builtin.c passdb/pdb_compat.c \
		passdb/util_unixsids.c passdb/lookup_sid.c \
		passdb/login_cache.c  passdb/pdb_smbpasswd.c passdb/pdb_tdb.c passdb/pdb_wbc_sam.c \
		lib/account_pol.c $(PRIVILEGES_OBJ) \
		lib/util_nscd.c lib/winbind_util.c $(SERVER_MUTEX_OBJ)

DEVEL_HELP_WEIRD_OBJ = modules/weird.c
CP850_OBJ = modules/CP850.c
CP437_OBJ = modules/CP437.c
CHARSET_MACOSXFS_OBJ = modules/charset_macosxfs.c

GROUPDB_OBJ = groupdb/mapping.c groupdb/mapping_tdb.c groupdb/mapping_ldb.c

PROFILE_OBJ = profile/profile.c
PROFILES_OBJ = utils/profiles.c \
	       $(LIBSAMBA_OBJ) \
	       $(PARAM_OBJ) \
               $(LIB_OBJ) $(LIB_DUMMY_OBJ) \
               $(POPT_LIB_OBJ)

OPLOCK_OBJ = smbd/oplock.c smbd/oplock_irix.c smbd/oplock_linux.c \
	     smbd/oplock_onefs.c

NOTIFY_OBJ = smbd/notify.c smbd/notify_inotify.c smbd/notify_internal.c

FNAME_UTIL_OBJ = smbd/filename_util.c

VFS_DEFAULT_OBJ = modules/vfs_default.c
VFS_AUDIT_OBJ = modules/vfs_audit.c
VFS_EXTD_AUDIT_OBJ = modules/vfs_extd_audit.c
VFS_FULL_AUDIT_OBJ = modules/vfs_full_audit.c
VFS_FAKE_PERMS_OBJ = modules/vfs_fake_perms.c
VFS_RECYCLE_OBJ = modules/vfs_recycle.c
VFS_NETATALK_OBJ = modules/vfs_netatalk.c
VFS_DEFAULT_QUOTA_OBJ = modules/vfs_default_quota.c
VFS_READONLY_OBJ = modules/vfs_readonly.c modules/getdate.c
VFS_CAP_OBJ = modules/vfs_cap.c
VFS_EXPAND_MSDFS_OBJ = modules/vfs_expand_msdfs.c
VFS_SHADOW_COPY_OBJ = modules/vfs_shadow_copy.c
VFS_SHADOW_COPY2_OBJ = modules/vfs_shadow_copy2.c
VFS_AFSACL_OBJ = modules/vfs_afsacl.c
VFS_XATTR_TDB_OBJ = modules/vfs_xattr_tdb.c
VFS_POSIXACL_OBJ = modules/vfs_posixacl.c
VFS_AIXACL_OBJ = modules/vfs_aixacl.c modules/vfs_aixacl_util.c
VFS_AIXACL2_OBJ = modules/vfs_aixacl2.c modules/vfs_aixacl_util.c modules/nfs4_acls.c
VFS_SOLARISACL_OBJ = modules/vfs_solarisacl.c
VFS_ZFSACL_OBJ = modules/vfs_zfsacl.c modules/nfs4_acls.c
VFS_HPUXACL_OBJ = modules/vfs_hpuxacl.c
VFS_IRIXACL_OBJ = modules/vfs_irixacl.c
VFS_TRU64ACL_OBJ = modules/vfs_tru64acl.c
VFS_CATIA_OBJ = modules/vfs_catia.c
VFS_STREAMS_XATTR_OBJ = modules/vfs_streams_xattr.c
VFS_STREAMS_DEPOT_OBJ = modules/vfs_streams_depot.c
VFS_CACHEPRIME_OBJ = modules/vfs_cacheprime.c
VFS_PREALLOC_OBJ = modules/vfs_prealloc.c
VFS_COMMIT_OBJ = modules/vfs_commit.c
VFS_GPFS_OBJ = modules/vfs_gpfs.c modules/gpfs.c modules/nfs4_acls.c
VFS_NOTIFY_FAM_OBJ = modules/vfs_notify_fam.c
VFS_READAHEAD_OBJ = modules/vfs_readahead.c
VFS_TSMSM_OBJ = modules/vfs_tsmsm.c
VFS_FILEID_OBJ = modules/vfs_fileid.c
VFS_AIO_FORK_OBJ = modules/vfs_aio_fork.c
VFS_PREOPEN_OBJ = modules/vfs_preopen.c
VFS_SYNCOPS_OBJ = modules/vfs_syncops.c
VFS_ACL_XATTR_OBJ = modules/vfs_acl_xattr.c
VFS_ACL_TDB_OBJ = modules/vfs_acl_tdb.c
VFS_SMB_TRAFFIC_ANALYZER_OBJ = modules/vfs_smb_traffic_analyzer.c
VFS_ONEFS_OBJ = modules/vfs_onefs.c modules/onefs_acl.c modules/onefs_system.c \
		modules/onefs_open.c modules/onefs_streams.c modules/onefs_dir.c \
		modules/onefs_cbrl.c modules/onefs_notify.c modules/onefs_config.c
VFS_ONEFS_SHADOW_COPY_OBJ = modules/vfs_onefs_shadow_copy.c modules/onefs_shadow_copy.c
PERFCOUNT_ONEFS_OBJ = modules/perfcount_onefs.c
PERFCOUNT_TEST_OBJ = modules/perfcount_test.c
VFS_DIRSORT_OBJ = modules/vfs_dirsort.c
VFS_SCANNEDONLY_OBJ = modules/vfs_scannedonly.c

PLAINTEXT_AUTH_OBJ = auth/pampass.c auth/pass_check.c

SLCACHE_OBJ = libsmb/samlogon_cache.c

DCUTIL_OBJ  = libsmb/namequery_dc.c libsmb/trustdom_cache.c libsmb/trusts_util.c libsmb/dsgetdcname.c

AUTH_BUILTIN_OBJ = auth/auth_builtin.c
AUTH_DOMAIN_OBJ = auth/auth_domain.c
AUTH_SAM_OBJ = auth/auth_sam.c
AUTH_SERVER_OBJ = auth/auth_server.c
AUTH_UNIX_OBJ = auth/auth_unix.c
AUTH_WINBIND_OBJ = auth/auth_winbind.c
AUTH_WBC_OBJ = auth/auth_wbc.c
AUTH_SCRIPT_OBJ = auth/auth_script.c
AUTH_NETLOGOND_OBJ = auth/auth_netlogond.c

AUTH_OBJ = auth/auth.c  $(AUTH_SAM_OBJ) $(AUTH_UNIX_OBJ) $(AUTH_WINBIND_OBJ) $(AUTH_WBC_OBJ) $(AUTH_SERVER_OBJ) $(AUTH_DOMAIN_OBJ) $(AUTH_BUILTIN_OBJ) $(AUTH_SCRIPT_OBJ) $(AUTH_NETLOGOND_OBJ) auth/auth_util.c auth/token_util.c \
	   auth/auth_compat.c auth/auth_ntlmssp.c \
	   $(PLAINTEXT_AUTH_OBJ) $(SLCACHE_OBJ) $(DCUTIL_OBJ)

MANGLE_OBJ = smbd/mangle.c smbd/mangle_hash.c smbd/mangle_hash2.c

SMBD_OBJ_MAIN = smbd/server.c

BUILDOPT_OBJ = smbd/build_options.c

SMBD_OBJ_SRV = smbd/files.c smbd/chgpasswd.c smbd/connection.c \
	       smbd/utmp.c smbd/session.c smbd/map_username.c \
               smbd/dfree.c smbd/dir.c smbd/password.c smbd/conn.c \
	       smbd/share_access.c smbd/fileio.c \
               smbd/ipc.c smbd/lanman.c smbd/negprot.c \
               smbd/message.c smbd/nttrans.c smbd/pipes.c \
               smbd/reply.c smbd/sesssetup.c smbd/trans2.c smbd/uid.c \
	       smbd/dosmode.c smbd/filename.c smbd/open.c smbd/close.c \
	       smbd/blocking.c smbd/sec_ctx.c smbd/srvstr.c \
	       smbd/vfs.c smbd/perfcount.c smbd/statcache.c smbd/seal.c \
               smbd/posix_acls.c lib/sysacls.c \
	       smbd/process.c smbd/service.c smbd/error.c \
	       printing/printfsp.c lib/sysquotas.c lib/sysquotas_linux.c \
	       lib/sysquotas_xfs.c lib/sysquotas_4A.c \
	       smbd/change_trust_pw.c smbd/fake_file.c \
	       smbd/quotas.c smbd/ntquotas.c $(AFS_OBJ) smbd/msdfs.c \
	       $(AFS_SETTOKEN_OBJ) smbd/aio.c smbd/statvfs.c \
	       smbd/dmapi.c smbd/signing.c \
	       smbd/file_access.c \
	       smbd/dnsregister.c smbd/globals.c \
	       smbd/smb2_server.c \
	       smbd/smb2_signing.c \
	       smbd/smb2_glue.c \
	       smbd/smb2_negprot.c \
	       smbd/smb2_sesssetup.c \
	       smbd/smb2_tcon.c \
	       smbd/smb2_create.c \
	       smbd/smb2_close.c \
	       smbd/smb2_flush.c \
	       smbd/smb2_read.c \
	       smbd/smb2_write.c \
	       smbd/smb2_lock.c \
	       smbd/smb2_ioctl.c \
	       smbd/smb2_keepalive.c \
	       smbd/smb2_find.c \
	       smbd/smb2_notify.c \
	       smbd/smb2_getinfo.c \
	       smbd/smb2_setinfo.c \
	       smbd/smb2_break.c \
	       ../libcli/smb/smb2_create_blob.c \
	       $(MANGLE_OBJ)  $(VFS_DEFAULT_OBJ) $(VFS_RECYCLE_OBJ) $(VFS_AUDIT_OBJ) $(VFS_EXTD_AUDIT_OBJ) $(VFS_FULL_AUDIT_OBJ) $(VFS_NETATALK_OBJ) $(VFS_FAKE_PERMS_OBJ) $(VFS_DEFAULT_QUOTA_OBJ) $(VFS_READONLY_OBJ) $(VFS_CAP_OBJ) $(VFS_EXPAND_MSDFS_OBJ) $(VFS_SHADOW_COPY_OBJ) $(VFS_SHADOW_COPY2_OBJ) $(VFS_XATTR_TDB_OBJ) $(VFS_CATIA_OBJ) $(VFS_STREAMS_XATTR_OBJ) $(VFS_STREAMS_DEPOT_OBJ) $(VFS_READAHEAD_OBJ) $(VFS_PREOPEN_OBJ) $(VFS_SYNCOPS_OBJ) $(VFS_ACL_XATTR_OBJ) $(VFS_ACL_TDB_OBJ) $(VFS_SMB_TRAFFIC_ANALYZER_OBJ) $(VFS_DIRSORT_OBJ) $(VFS_SCANNEDONLY_OBJ)

SMBD_OBJ_BASE = $(PARAM_WITHOUT_REG_OBJ) $(SMBD_OBJ_SRV) $(LIBSMB_OBJ) \
		$(RPC_SERVER_OBJ) $(RPC_PARSE_OBJ) \
		$(LOCKING_OBJ) $(PASSDB_OBJ) $(PRINTING_OBJ) $(PROFILE_OBJ) \
		$(LIB_OBJ) $(PRINTBACKEND_OBJ) $(OPLOCK_OBJ) \
		$(NOTIFY_OBJ) $(FNAME_UTIL_OBJ) $(GROUPDB_OBJ) $(AUTH_OBJ) \
		$(LIBMSRPC_OBJ) $(LIBMSRPC_GEN_OBJ) $(AVAHI_OBJ) \
		$(LIBADS_OBJ) $(KRBCLIENT_OBJ) $(LIBADS_SERVER_OBJ) \
		$(REG_FULL_OBJ) $(POPT_LIB_OBJ) $(BUILDOPT_OBJ) \
		$(SMBLDAP_OBJ) $(LDB_OBJ) $(LIBNET_OBJ) \
		$(LIBSMBCONF_OBJ) \
		$(PRIVILEGES_BASIC_OBJ)

PRINTING_OBJ = printing/pcap.c printing/print_svid.c printing/print_aix.c \
               printing/print_cups.c printing/print_generic.c \
               printing/lpq_parse.c printing/load.c \
               printing/print_iprint.c librpc/gen_ndr/ndr_printcap.c

PRINTBASE_OBJ = printing/notify.c printing/printing_db.c
PRINTBACKEND_OBJ = printing/printing.c printing/nt_printing.c $(PRINTBASE_OBJ)

SMBD_OBJ = $(SMBD_OBJ_BASE) $(SMBD_OBJ_MAIN)

NMBD_OBJ1 = nmbd/asyncdns.c nmbd/nmbd.c nmbd/nmbd_become_dmb.c \
            nmbd/nmbd_become_lmb.c nmbd/nmbd_browserdb.c \
            nmbd/nmbd_browsesync.c nmbd/nmbd_elections.c \
            nmbd/nmbd_incomingdgrams.c nmbd/nmbd_incomingrequests.c \
            nmbd/nmbd_lmhosts.c nmbd/nmbd_logonnames.c nmbd/nmbd_mynames.c \
            nmbd/nmbd_namelistdb.c nmbd/nmbd_namequery.c \
            nmbd/nmbd_nameregister.c nmbd/nmbd_namerelease.c \
            nmbd/nmbd_nodestatus.c nmbd/nmbd_packets.c \
            nmbd/nmbd_processlogon.c nmbd/nmbd_responserecordsdb.c \
            nmbd/nmbd_sendannounce.c nmbd/nmbd_serverlistdb.c \
            nmbd/nmbd_subnetdb.c nmbd/nmbd_winsproxy.c nmbd/nmbd_winsserver.c \
            nmbd/nmbd_workgroupdb.c nmbd/nmbd_synclists.c smbd/connection.c

NMBD_OBJ = $(NMBD_OBJ1) $(PARAM_OBJ) $(LIBSMB_OBJ) $(LDB_OBJ) $(KRBCLIENT_OBJ) \
           $(PROFILE_OBJ) $(LIB_NONSMBD_OBJ) $(POPT_LIB_OBJ) \
	   $(LIBNDR_GEN_OBJ0)

SWAT_OBJ1 = web/cgi.c web/diagnose.c web/startstop.c web/statuspage.c \
           web/swat.c web/neg_lang.c

SWAT_OBJ = $(SWAT_OBJ1) $(PARAM_OBJ) $(PRINTING_OBJ) $(PRINTBASE_OBJ) $(LIBSMB_OBJ) \
	   $(LOCKING_OBJ) $(PASSDB_OBJ) $(KRBCLIENT_OBJ) \
	   $(LIB_NONSMBD_OBJ) $(GROUPDB_OBJ) $(PLAINTEXT_AUTH_OBJ) \
	   $(POPT_LIB_OBJ) $(SMBLDAP_OBJ) $(RPC_PARSE_OBJ) $(LIBMSRPC_GEN_OBJ) $(LIBMSRPC_OBJ) \
           $(PASSCHANGE_OBJ) $(LDB_OBJ) $(FNAME_UTIL_OBJ)

STATUS_OBJ = utils/status.c utils/status_profile.c \
	     $(LOCKING_OBJ) $(PARAM_OBJ) \
             $(PROFILE_OBJ) $(LIB_NONSMBD_OBJ) $(POPT_LIB_OBJ) \
	     $(LIBSAMBA_OBJ) $(FNAME_UTIL_OBJ)

SMBCONTROL_OBJ = utils/smbcontrol.c $(LOCKING_OBJ) $(PARAM_OBJ) \
	$(PROFILE_OBJ) $(LIB_NONSMBD_OBJ) $(POPT_LIB_OBJ) \
	$(LIBSAMBA_OBJ) $(FNAME_UTIL_OBJ) \
	$(PRINTBASE_OBJ)

SMBTREE_OBJ = utils/smbtree.c $(PARAM_OBJ) \
             $(PROFILE_OBJ) $(LIB_NONSMBD_OBJ) $(LIBSMB_OBJ) \
	     $(KRBCLIENT_OBJ) $(POPT_LIB_OBJ) \
             $(RPC_CLIENT_OBJ) ../librpc/rpc/binding.c $(RPC_PARSE_OBJ2) \
             $(RPC_CLIENT_OBJ1) \
	     $(PASSDB_OBJ) $(SMBLDAP_OBJ) $(LDB_OBJ) $(GROUPDB_OBJ) \
	     $(LIBMSRPC_GEN_OBJ)

TESTPARM_OBJ = utils/testparm.c \
               $(PARAM_OBJ) $(LIB_NONSMBD_OBJ) $(POPT_LIB_OBJ) \
	       $(LIBSAMBA_OBJ)

TEST_LP_LOAD_OBJ = param/test_lp_load.c \
		   $(PARAM_OBJ) $(LIB_NONSMBD_OBJ) \
		   $(POPT_LIB_OBJ) $(LIBSAMBA_OBJ)

PASSWD_UTIL_OBJ = utils/passwd_util.c

SMBPASSWD_OBJ = utils/smbpasswd.c $(PASSWD_UTIL_OBJ) $(PASSCHANGE_OBJ) \
		$(PARAM_OBJ) $(LIBSMB_OBJ) $(PASSDB_OBJ) \
		$(GROUPDB_OBJ) $(LIB_NONSMBD_OBJ) $(KRBCLIENT_OBJ) \
		$(POPT_LIB_OBJ) $(SMBLDAP_OBJ) $(RPC_PARSE_OBJ) \
		$(LIBMSRPC_GEN_OBJ) $(LIBMSRPC_OBJ) $(LDB_OBJ)

PDBEDIT_OBJ = utils/pdbedit.c $(PASSWD_UTIL_OBJ) $(PARAM_OBJ) $(PASSDB_OBJ) \
		$(LIBSAMBA_OBJ) $(LIBTSOCKET_OBJ) \
		$(LIB_NONSMBD_OBJ) $(GROUPDB_OBJ) \
		$(POPT_LIB_OBJ) $(SMBLDAP_OBJ) ../lib/util/asn1.c \
		$(LDB_OBJ)

SMBGET_OBJ = utils/smbget.c $(POPT_LIB_OBJ) $(LIBSMBCLIENT_OBJ1)

DISPLAY_SEC_OBJ= ../libcli/security/display_sec.c

RPCCLIENT_OBJ1 = rpcclient/rpcclient.c rpcclient/cmd_lsarpc.c \
	         rpcclient/cmd_samr.c rpcclient/cmd_spoolss.c \
		 rpcclient/cmd_netlogon.c rpcclient/cmd_srvsvc.c \
		 rpcclient/cmd_dfs.c rpcclient/cmd_epmapper.c \
		 rpcclient/cmd_dssetup.c rpcclient/cmd_echo.c \
		 rpcclient/cmd_shutdown.c rpcclient/cmd_test.c \
		 rpcclient/cmd_wkssvc.c rpcclient/cmd_ntsvcs.c \
		 rpcclient/cmd_drsuapi.c rpcclient/cmd_eventlog.c \
		 $(DISPLAY_SEC_OBJ)

RPCCLIENT_OBJ = $(RPCCLIENT_OBJ1) \
             $(PARAM_OBJ) $(LIBSMB_OBJ) $(LIB_NONSMBD_OBJ) \
             $(RPC_PARSE_OBJ) $(PASSDB_OBJ) $(LIBMSRPC_GEN_OBJ) $(LIBMSRPC_OBJ) \
             $(READLINE_OBJ) $(GROUPDB_OBJ) $(KRBCLIENT_OBJ) \
	     $(LIBADS_OBJ) $(POPT_LIB_OBJ) \
	     $(SMBLDAP_OBJ) $(DCUTIL_OBJ) $(LDB_OBJ) 

PAM_WINBIND_OBJ = ../nsswitch/pam_winbind.c localedir.c $(WBCOMMON_OBJ) \
		  $(LIBREPLACE_OBJ) $(INIPARSER_OBJ)

LIBSMBCLIENT_THREAD_OBJ = \
			libsmb/libsmb_thread_impl.c \
			libsmb/libsmb_thread_posix.c

LIBSMBCLIENT_OBJ0 = \
		    libsmb/libsmb_cache.c \
		    libsmb/libsmb_compat.c \
		    libsmb/libsmb_context.c \
		    libsmb/libsmb_dir.c \
		    libsmb/libsmb_file.c \
		    libsmb/libsmb_misc.c \
		    libsmb/libsmb_path.c \
		    libsmb/libsmb_printjob.c \
		    libsmb/libsmb_server.c \
		    libsmb/libsmb_stat.c \
		    libsmb/libsmb_xattr.c \
		    libsmb/libsmb_setget.c

LIBSMBCLIENT_OBJ1 = $(LIBSMBCLIENT_OBJ0) \
		    $(PARAM_OBJ) $(LIB_NONSMBD_OBJ) \
		    $(LIBSMB_OBJ) $(KRBCLIENT_OBJ) \
		    $(LIBMSRPC_OBJ) $(LIBMSRPC_GEN_OBJ) $(RPC_PARSE_OBJ) \
		    $(PASSDB_OBJ) $(SMBLDAP_OBJ) $(GROUPDB_OBJ) $(LDB_OBJ)

LIBSMBCLIENT_OBJ = $(LIBSMBCLIENT_OBJ1)

# This shared library is intended for linking with unit test programs
# to test Samba internals.  It's called libbigballofmud.so to
# discourage casual usage.

LIBBIGBALLOFMUD_MAJOR = 0

LIBBIGBALLOFMUD_OBJ = $(PARAM_OBJ) $(LIB_NONSMBD_OBJ) \
	$(LIBSMB_OBJ) $(LIBMSRPC_OBJ) $(LIBMSRPC_GEN_OBJ) $(RPC_PARSE_OBJ) $(PASSDB_OBJ) \
	$(GROUPDB_OBJ) $(KRBCLIENT_OBJ) $(SMBLDAP_OBJ) $(LDB_OBJ)

CLIENT_OBJ1 = client/client.c client/clitar.c $(RPC_CLIENT_OBJ) \
	      ../librpc/rpc/binding.c \
	      client/dnsbrowse.c \
	      $(RPC_CLIENT_OBJ1) \
	      $(RPC_PARSE_OBJ2)

CLIENT_OBJ = $(CLIENT_OBJ1) $(PARAM_OBJ) $(LIBSMB_OBJ) \
	     $(LIB_NONSMBD_OBJ) $(KRBCLIENT_OBJ) $(LIBMSRPC_GEN_OBJ) \
             $(READLINE_OBJ) $(POPT_LIB_OBJ) \
             $(PASSDB_OBJ) $(SMBLDAP_OBJ) $(GROUPDB_OBJ) $(LDB_OBJ) \
	     $(DISPLAY_SEC_OBJ)

LIBSMBCONF_OBJ = ../lib/smbconf/smbconf.c \
		 ../lib/smbconf/smbconf_util.c \
		 ../lib/smbconf/smbconf_txt.c \
		 lib/smbconf/smbconf_reg.c \
		 lib/smbconf/smbconf_init.c

SMBCONFTORT_OBJ0 = lib/smbconf/testsuite.c

SMBCONFTORT_OBJ = $(SMBCONFTORT_OBJ0) \
		  $(LIB_NONSMBD_OBJ) \
		  $(PARAM_OBJ) \
		  $(LIBSMB_ERR_OBJ) \
		  $(POPT_LIB_OBJ)

LIBNET_OBJ = libnet/libnet_join.c \
	     libnet/libnet_keytab.c \
	     librpc/gen_ndr/ndr_libnet_join.c

LIBNET_DSSYNC_OBJ = libnet/libnet_dssync.c \
		    libnet/libnet_dssync_keytab.c \
		    ../libcli/drsuapi/repl_decrypt.c

LIBNET_SAMSYNC_OBJ = libnet/libnet_samsync.c \
		     libnet/libnet_samsync_ldif.c \
		     libnet/libnet_samsync_passdb.c \
		     libnet/libnet_samsync_display.c \
		     libnet/libnet_samsync_keytab.c \
		     ../libcli/samsync/decrypt.c

NET_OBJ1 = utils/net.c utils/net_ads.c utils/net_help.c \
	   utils/net_rap.c utils/net_rpc.c utils/net_rpc_samsync.c \
	   utils/net_rpc_join.c utils/net_time.c utils/net_lookup.c \
	   utils/net_cache.c utils/net_groupmap.c utils/net_idmap.c \
	   utils/net_status.c utils/net_rpc_printer.c utils/net_rpc_rights.c \
	   utils/net_rpc_service.c utils/net_rpc_registry.c utils/net_usershare.c \
	   utils/netlookup.c utils/net_sam.c utils/net_rpc_shell.c \
	   utils/net_util.c utils/net_rpc_sh_acct.c utils/net_rpc_audit.c \
	   $(PASSWD_UTIL_OBJ) utils/net_dns.c utils/net_ads_gpo.c \
	   utils/net_conf.c utils/net_join.c utils/net_user.c \
	   utils/net_group.c utils/net_file.c utils/net_registry.c \
	   auth/token_util.c utils/net_dom.c utils/net_share.c \
	   utils/net_g_lock.c \
	   utils/net_eventlog.c

# these are not processed by make proto
NET_OBJ2 = utils/net_registry_util.c utils/net_help_common.c

NET_OBJ = $(NET_OBJ1) \
	  $(NET_OBJ2) \
	  $(PARAM_WITHOUT_REG_OBJ) $(LIBSMB_OBJ) \
	  $(RPC_PARSE_OBJ) $(PASSDB_OBJ) $(GROUPDB_OBJ) \
	  $(KRBCLIENT_OBJ) $(LIB_NONSMBD_OBJ) $(LIBADDNS_OBJ0) \
	  $(LIBMSRPC_OBJ) $(LIBMSRPC_GEN_OBJ) \
	  $(LIBADS_OBJ) $(LIBADS_SERVER_OBJ) $(POPT_LIB_OBJ) \
	  $(SMBLDAP_OBJ) $(DCUTIL_OBJ) \
	  $(AFS_OBJ) $(AFS_SETTOKEN_OBJ) $(READLINE_OBJ) \
	  $(LDB_OBJ) $(LIBGPO_OBJ) $(INIPARSER_OBJ) $(DISPLAY_SEC_OBJ) \
	  $(REG_SMBCONF_OBJ) \
	  $(LIBNET_OBJ) $(LIBNET_DSSYNC_OBJ) $(LIBNET_SAMSYNC_OBJ) \
	  $(LIBSMBCONF_OBJ) \
	  $(PRIVILEGES_BASIC_OBJ) \
	  $(LIB_EVENTLOG_OBJ) localedir.c

CUPS_OBJ = client/smbspool.c $(PARAM_OBJ) $(LIBSMB_OBJ) $(LDB_OBJ) \
	  $(LIB_NONSMBD_OBJ) $(KRBCLIENT_OBJ) $(POPT_LIB_OBJ) \
	  $(LIBNDR_GEN_OBJ0)

CIFS_MOUNT_OBJ = ../client/mount.cifs.c ../client/mtab.c

CIFS_UMOUNT_OBJ = ../client/umount.cifs.c ../client/mtab.c

CIFS_UPCALL_OBJ = ../client/cifs.upcall.c

NMBLOOKUP_OBJ = utils/nmblookup.c $(PARAM_OBJ) $(LIBNMB_OBJ) \
               $(LIB_NONSMBD_OBJ) $(POPT_LIB_OBJ) $(LIBSAMBA_OBJ)

SMBTORTURE_OBJ1 = torture/torture.c torture/nbio.c torture/scanner.c torture/utable.c \
		torture/denytest.c torture/mangle_test.c

SMBTORTURE_OBJ = $(SMBTORTURE_OBJ1) $(PARAM_OBJ) $(TLDAP_OBJ) \
	$(LIBSMB_OBJ) $(LDB_OBJ) $(KRBCLIENT_OBJ) $(LIB_NONSMBD_OBJ) \
	bin/libwbclient.a \
	$(LIBNDR_GEN_OBJ0)

MASKTEST_OBJ = torture/masktest.c $(PARAM_OBJ) $(LIBSMB_OBJ) $(LDB_OBJ) $(KRBCLIENT_OBJ) \
                 $(LIB_NONSMBD_OBJ) \
		 $(LIBNDR_GEN_OBJ0)

MSGTEST_OBJ = torture/msgtest.c $(PARAM_OBJ) $(LIBSMB_OBJ) $(LDB_OBJ) $(KRBCLIENT_OBJ) \
                 $(LIB_NONSMBD_OBJ) \
		 $(LIBNDR_GEN_OBJ0)

LOCKTEST_OBJ = torture/locktest.c $(PARAM_OBJ) $(LOCKING_OBJ) $(KRBCLIENT_OBJ) \
               $(LIBSMB_OBJ) $(LDB_OBJ) $(LIB_NONSMBD_OBJ) \
               $(LIBNDR_GEN_OBJ0) $(FNAME_UTIL_OBJ)

NSSTEST_OBJ = torture/nsstest.c $(LIBSAMBAUTIL_OBJ)

PDBTEST_OBJ = torture/pdbtest.c $(PARAM_OBJ) $(LIBSMB_OBJ) $(KRBCLIENT_OBJ) \
		$(LIB_NONSMBD_OBJ) $(PASSDB_OBJ) $(GROUPDB_OBJ) \
		$(SMBLDAP_OBJ) $(POPT_LIB_OBJ) $(LDB_OBJ) \
		$(LIBNDR_GEN_OBJ0)

VFSTEST_OBJ = torture/cmd_vfs.c torture/vfstest.c $(SMBD_OBJ_BASE) $(READLINE_OBJ)

SMBICONV_OBJ = $(PARAM_OBJ) torture/smbiconv.c $(LIB_NONSMBD_OBJ) $(POPT_LIB_OBJ) $(LIBSAMBA_OBJ)

LOG2PCAP_OBJ = utils/log2pcaphex.c

LOCKTEST2_OBJ = torture/locktest2.c $(PARAM_OBJ) $(LOCKING_OBJ) $(LIBSMB_OBJ) $(LDB_OBJ) \
		$(KRBCLIENT_OBJ) $(LIB_NONSMBD_OBJ) \
		$(LIBNDR_GEN_OBJ0) $(FNAME_UTIL_OBJ)

SMBCACLS_OBJ = utils/smbcacls.c $(PARAM_OBJ) $(LIBSMB_OBJ) \
		$(KRBCLIENT_OBJ) $(LIB_NONSMBD_OBJ) $(RPC_PARSE_OBJ) \
		$(PASSDB_OBJ) $(GROUPDB_OBJ) $(LIBMSRPC_OBJ) $(LIBMSRPC_GEN_OBJ) \
		$(POPT_LIB_OBJ) $(DCUTIL_OBJ) $(LIBADS_OBJ) $(SMBLDAP_OBJ) $(LDB_OBJ)

SMBCQUOTAS_OBJ = utils/smbcquotas.c $(LIBSMB_OBJ) $(KRBCLIENT_OBJ) \
		$(PARAM_OBJ) \
		$(LIB_NONSMBD_OBJ) $(RPC_PARSE_OBJ) \
		$(LIBMSRPC_OBJ) $(LIBMSRPC_GEN_OBJ) $(POPT_LIB_OBJ) \
		$(PASSDB_OBJ) $(SMBLDAP_OBJ) $(GROUPDB_OBJ) $(LDB_OBJ)

EVTLOGADM_OBJ0	= utils/eventlogadm.c

EVTLOGADM_OBJ	= $(EVTLOGADM_OBJ0) $(PARAM_OBJ) $(LIB_NONSMBD_OBJ) \
		$(LIBSAMBA_OBJ) \
	        registry/reg_eventlog.c $(LIB_EVENTLOG_OBJ) \
		../librpc/gen_ndr/ndr_eventlog.c \
		../librpc/gen_ndr/ndr_lsa.c

SHARESEC_OBJ0 = utils/sharesec.c
SHARESEC_OBJ  = $(SHARESEC_OBJ0) $(PARAM_OBJ) $(LIB_NONSMBD_OBJ) \
		$(LIBSAMBA_OBJ) \
                $(POPT_LIB_OBJ)

TALLOCTORT_OBJ = ../lib/talloc/testsuite.c ../lib/talloc/testsuite_main.c \
		$(PARAM_OBJ) $(LIB_NONSMBD_OBJ) $(LIBSAMBA_OBJ)

REPLACETORT_OBJ = ./../lib/replace/test/testsuite.c \
		./../lib/replace/test/getifaddrs.c \
		./../lib/replace/test/os2_delete.c \
		./../lib/replace/test/strptime.c \
		./../lib/replace/test/main.c \
		$(LIBREPLACE_OBJ) $(SOCKET_WRAPPER_OBJ)

DEBUG2HTML_OBJ = utils/debug2html.c utils/debugparse.c

SMBFILTER_OBJ = utils/smbfilter.c $(PARAM_OBJ) $(LIBSMB_OBJ) $(LDB_OBJ) \
                 $(LIB_NONSMBD_OBJ) $(KRBCLIENT_OBJ) \
		 $(LIBNDR_GEN_OBJ0)

WINBIND_WINS_NSS_OBJ = ../nsswitch/wins.c $(PARAM_OBJ) \
	$(LIBSMB_OBJ) $(LIB_NONSMBD_OBJ) $(NSSWINS_OBJ) $(KRBCLIENT_OBJ) \
	$(LIBNDR_GEN_OBJ0) $(LDB_OBJ)

PAM_SMBPASS_OBJ_0 = pam_smbpass/pam_smb_auth.c pam_smbpass/pam_smb_passwd.c \
		pam_smbpass/pam_smb_acct.c pam_smbpass/support.c ../lib/util/asn1.c
PAM_SMBPASS_OBJ = $(PAM_SMBPASS_OBJ_0) $(PARAM_OBJ) $(LIB_NONSMBD_OBJ) $(PASSDB_OBJ) $(GROUPDB_OBJ) \
		$(SMBLDAP_OBJ) $(LIBSAMBA_OBJ) \
		$(LDB_OBJ) $(LIBTSOCKET_OBJ)

IDMAP_OBJ     = winbindd/idmap.c winbindd/idmap_util.c  winbindd/idmap_tdb.c winbindd/idmap_passdb.c winbindd/idmap_nss.c

NSS_INFO_OBJ = winbindd/nss_info.c  winbindd/nss_info_template.c

IDMAP_HASH_OBJ = \
		winbindd/idmap_hash/idmap_hash.c \
		winbindd/idmap_hash/mapfile.c

IDMAP_ADEX_OBJ = \
		winbindd/idmap_adex/idmap_adex.c \
		winbindd/idmap_adex/cell_util.c \
		winbindd/idmap_adex/likewise_cell.c \
		winbindd/idmap_adex/provider_unified.c \
		winbindd/idmap_adex/gc_util.c \
		winbindd/idmap_adex/domain_util.c

WINBINDD_OBJ1 = \
		winbindd/winbindd.c       \
		winbindd/winbindd_group.c \
		winbindd/winbindd_util.c  \
		winbindd/winbindd_cache.c \
		winbindd/winbindd_pam.c   \
		winbindd/winbindd_misc.c  \
		winbindd/winbindd_cm.c    \
		winbindd/winbindd_wins.c  \
		winbindd/winbindd_rpc.c   \
		winbindd/winbindd_reconnect.c \
		winbindd/winbindd_ads.c   \
		winbindd/winbindd_passdb.c \
		winbindd/winbindd_dual.c  \
		winbindd/winbindd_dual_ndr.c  \
		winbindd/winbindd_dual_srv.c  \
		librpc/gen_ndr/cli_wbint.c \
		librpc/gen_ndr/srv_wbint.c \
		librpc/gen_ndr/ndr_wbint.c \
		winbindd/winbindd_async.c \
		winbindd/winbindd_creds.c \
		winbindd/winbindd_cred_cache.c \
		winbindd/winbindd_ccache_access.c \
		winbindd/winbindd_domain.c \
		winbindd/winbindd_idmap.c \
		winbindd/winbindd_locator.c \
		winbindd/winbindd_ndr.c \
		winbindd/wb_ping.c \
		winbindd/wb_lookupsid.c \
		winbindd/wb_lookupname.c \
		winbindd/wb_sid2uid.c \
		winbindd/wb_sid2gid.c \
		winbindd/wb_uid2sid.c \
		winbindd/wb_gid2sid.c \
		winbindd/wb_queryuser.c \
		winbindd/wb_lookupuseraliases.c \
		winbindd/wb_lookupusergroups.c \
		winbindd/wb_getpwsid.c \
		winbindd/wb_gettoken.c \
		winbindd/wb_seqnum.c \
		winbindd/wb_seqnums.c \
		winbindd/wb_group_members.c \
		winbindd/wb_getgrsid.c \
		winbindd/wb_query_user_list.c \
		winbindd/wb_fill_pwent.c \
		winbindd/wb_next_pwent.c \
		winbindd/wb_next_grent.c \
		winbindd/wb_dsgetdcname.c \
		winbindd/winbindd_lookupsid.c \
		winbindd/winbindd_lookupname.c \
		winbindd/winbindd_sid_to_uid.c \
		winbindd/winbindd_sid_to_gid.c \
		winbindd/winbindd_uid_to_sid.c \
		winbindd/winbindd_gid_to_sid.c \
		winbindd/winbindd_allocate_uid.c \
		winbindd/winbindd_allocate_gid.c \
		winbindd/winbindd_getpwsid.c \
		winbindd/winbindd_getpwnam.c \
		winbindd/winbindd_getpwuid.c \
		winbindd/winbindd_getsidaliases.c \
		winbindd/winbindd_getuserdomgroups.c \
		winbindd/winbindd_getgroups.c \
		winbindd/winbindd_show_sequence.c \
		winbindd/winbindd_getgrgid.c \
		winbindd/winbindd_getgrnam.c \
		winbindd/winbindd_getusersids.c \
		winbindd/winbindd_lookuprids.c \
		winbindd/winbindd_setpwent.c \
		winbindd/winbindd_getpwent.c \
		winbindd/winbindd_endpwent.c \
		winbindd/winbindd_setgrent.c \
		winbindd/winbindd_getgrent.c \
		winbindd/winbindd_endgrent.c \
		winbindd/winbindd_dsgetdcname.c \
		winbindd/winbindd_getdcname.c \
		winbindd/winbindd_list_users.c \
		winbindd/winbindd_list_groups.c \
		winbindd/winbindd_check_machine_acct.c \
		winbindd/winbindd_change_machine_acct.c \
		winbindd/winbindd_ping_dc.c \
		winbindd/winbindd_set_mapping.c \
		winbindd/winbindd_remove_mapping.c \
		winbindd/winbindd_set_hwm.c \
		auth/token_util.c \
		../nsswitch/libwbclient/wb_reqtrans.c \
		smbd/connection.c

WINBINDD_OBJ = \
		$(WINBINDD_OBJ1) $(PASSDB_OBJ) $(GROUPDB_OBJ) \
		$(PARAM_OBJ) $(LIB_NONSMBD_OBJ) \
		$(LIBSMB_OBJ) $(LIBMSRPC_OBJ) $(LIBMSRPC_GEN_OBJ) $(RPC_PARSE_OBJ) \
		$(PROFILE_OBJ) $(SLCACHE_OBJ) $(SMBLDAP_OBJ) \
		$(LIBADS_OBJ) $(KRBCLIENT_OBJ) $(POPT_LIB_OBJ) \
		$(DCUTIL_OBJ) $(IDMAP_OBJ) $(NSS_INFO_OBJ) \
		$(AFS_OBJ) $(AFS_SETTOKEN_OBJ) \
		$(LIBADS_SERVER_OBJ) $(LDB_OBJ) \
		$(TDB_VALIDATE_OBJ)

WBINFO_OBJ = ../nsswitch/wbinfo.c $(LIBSAMBA_OBJ) $(PARAM_OBJ) $(LIB_NONSMBD_OBJ) \
		$(POPT_LIB_OBJ) $(AFS_SETTOKEN_OBJ) \
		lib/winbind_util.c $(WBCOMMON_OBJ)

WINBIND_NSS_OBJ = $(WBCOMMON_OBJ) $(LIBREPLACE_OBJ) 

LDB_COMMON_OBJ=lib/ldb/common/ldb.c lib/ldb/common/ldb_ldif.c \
          lib/ldb/common/ldb_parse.c lib/ldb/common/ldb_msg.c lib/ldb/common/ldb_utf8.c \
          lib/ldb/common/ldb_debug.c lib/ldb/common/ldb_modules.c \
          lib/ldb/common/ldb_dn.c lib/ldb/common/ldb_match.c lib/ldb/common/ldb_attributes.c \
          lib/ldb/common/attrib_handlers.c lib/ldb/common/ldb_controls.c lib/ldb/common/qsort.c

LDB_TDB_OBJ=lib/ldb/ldb_tdb/ldb_tdb.c \
       lib/ldb/ldb_tdb/ldb_pack.c lib/ldb/ldb_tdb/ldb_search.c lib/ldb/ldb_tdb/ldb_index.c \
       lib/ldb/ldb_tdb/ldb_cache.c lib/ldb/ldb_tdb/ldb_tdb_wrap.c

LDB_MODULES_OBJ=lib/ldb/modules/operational.c lib/ldb/modules/rdn_name.c \
          lib/ldb/modules/objectclass.c \
          lib/ldb/modules/paged_results.c lib/ldb/modules/sort.c lib/ldb/modules/asq.c

# enabled in configure.in
LDB_LDAP_OBJ=

LDB_OBJ = ${LDB_COMMON_OBJ} ${LDB_TDB_OBJ} ${LDB_LDAP_OBJ} ${LDB_MODULES_OBJ}

LDB_CMDLINE_OBJ = $(PARAM_OBJ) \
	  $(POPT_LIB_OBJ) $(LIB_OBJ) $(LIB_DUMMY_OBJ) $(LIBSMB_ERR_OBJ0) $(LIBSMB_ERR_OBJ1) \
	  $(RPC_PARSE_OBJ1) $(SECRETS_OBJ) \
	  $(LDB_OBJ) lib/ldb/tools/cmdline.c 


LDBEDIT_OBJ = $(LDB_CMDLINE_OBJ) lib/ldb/tools/ldbedit.c
LDBSEARCH_OBJ = $(LDB_CMDLINE_OBJ) lib/ldb/tools/ldbsearch.c
LDBADD_OBJ = $(LDB_CMDLINE_OBJ) lib/ldb/tools/ldbadd.c
LDBDEL_OBJ = $(LDB_CMDLINE_OBJ) lib/ldb/tools/ldbdel.c
LDBMODIFY_OBJ = $(LDB_CMDLINE_OBJ) lib/ldb/tools/ldbmodify.c
LDBRENAME_OBJ = $(LDB_CMDLINE_OBJ) lib/ldb/tools/ldbrename.c

WINBIND_KRB5_LOCATOR_OBJ1 = ../nsswitch/winbind_krb5_locator.c
WINBIND_KRB5_LOCATOR_OBJ = $(WINBIND_KRB5_LOCATOR_OBJ1) $(LIBREPLACE_OBJ)

POPT_OBJ=../lib/popt/findme.c ../lib/popt/popt.c ../lib/popt/poptconfig.c \
          ../lib/popt/popthelp.c ../lib/popt/poptparse.c

INIPARSER_OBJ = iniparser_build/iniparser.c iniparser_build/dictionary.c \
		iniparser_build/strlib.c

TDBBACKUP_OBJ = ../lib/tdb/tools/tdbbackup.c $(LIBREPLACE_OBJ) \
	$(SOCKET_WRAPPER_OBJ)

TDBTOOL_OBJ = ../lib/tdb/tools/tdbtool.c $(LIBREPLACE_OBJ) \
	$(SOCKET_WRAPPER_OBJ)

TDBDUMP_OBJ = ../lib/tdb/tools/tdbdump.c $(LIBREPLACE_OBJ) \
	$(SOCKET_WRAPPER_OBJ)

TDBTORTURE_OBJ = ../lib/tdb/tools/tdbtorture.c $(LIBREPLACE_OBJ) \
	$(SOCKET_WRAPPER_OBJ)


NTLM_AUTH_OBJ1 = utils/ntlm_auth.c utils/ntlm_auth_diagnostics.c

NTLM_AUTH_OBJ = ${NTLM_AUTH_OBJ1} $(LIBSAMBA_OBJ) $(POPT_LIB_OBJ) \
		../lib/util/asn1.c ../libcli/auth/spnego_parse.c libsmb/clikrb5.c libads/kerberos.c \
		$(LIBADS_SERVER_OBJ) \
		$(PASSDB_OBJ) $(LIBTSOCKET_OBJ) $(GROUPDB_OBJ) \
		$(SMBLDAP_OBJ) $(LIBNMB_OBJ) \
		$(LDB_OBJ) $(WBCOMMON_OBJ) $(SLCACHE_OBJ) \
		$(LIBNDR_GEN_OBJ0) $(LIBNDR_GEN_OBJ1) $(INIPARSER_OBJ)


VLP_OBJ = printing/tests/vlp.c \
	  ../lib/util/util_tdb.c \
	  $(LIBSAMBAUTIL_OBJ) \
	  param/util.c

RPC_OPEN_TCP_OBJ = torture/rpc_open_tcp.c \
		   $(LIBSMB_OBJ) \
		   $(PARAM_OBJ) \
		   $(PASSDB_OBJ) \
		   $(SMBLDAP_OBJ) $(LDB_OBJ) $(GROUPDB_OBJ) \
		   $(LIB_NONSMBD_OBJ) \
		   $(KRBCLIENT_OBJ) \
		   $(RPC_PARSE_OBJ2) \
		   $(RPC_CLIENT_OBJ1) \
		   rpc_client/cli_pipe.c \
		   ../librpc/rpc/binding.c \
		   $(LIBMSRPC_GEN_OBJ)

DBWRAP_TOOL_OBJ = utils/dbwrap_tool.c \
		  $(PARAM_OBJ) \
		  $(LIB_NONSMBD_OBJ) \
		  $(LIBSAMBA_OBJ)
		  
		  
		  
#-------------------------------------------------------------------
#
# libtalloc
#
#-------------------------------------------------------------------

LIBTALLOC_OBJ0 =  ../lib/talloc/talloc.c
LIBTALLOC_OBJ = $(LIBTALLOC_OBJ0) 


#-------------------------------------------------------------------
#
# libtdb
#
#-------------------------------------------------------------------

LIBTDB_OBJ0 =  ../lib/tdb/common/tdb.c ../lib/tdb/common/dump.c ../lib/tdb/common/transaction.c ../lib/tdb/common/error.c ../lib/tdb/common/traverse.c ../lib/tdb/common/freelist.c ../lib/tdb/common/freelistcheck.c ../lib/tdb/common/io.c ../lib/tdb/common/lock.c ../lib/tdb/common/open.c ../lib/tdb/common/check.c
LIBTDB_OBJ = $(LIBTDB_OBJ0) 


#-------------------------------------------------------------------
#
# libwbclient
#
#-------------------------------------------------------------------

LIBWBCLIENT_OBJ0 = ../nsswitch/libwbclient/wbclient.c \
		  ../nsswitch/libwbclient/wbc_util.c \
		  ../nsswitch/libwbclient/wbc_pwd.c \
		  ../nsswitch/libwbclient/wbc_idmap.c \
		  ../nsswitch/libwbclient/wbc_sid.c \
		  ../nsswitch/libwbclient/wbc_guid.c \
		  ../nsswitch/libwbclient/wbc_pam.c \
		  ../nsswitch/libwbclient/wb_reqtrans.c \
		  ../nsswitch/libwbclient/wbc_async.c

LIBWBCLIENT_OBJ = $(LIBWBCLIENT_OBJ0) \
		  $(WBCOMMON_OBJ) \
		  $(SOCKET_WRAPPER_OBJ)  


ZLIB_OBJS = ../lib/zlib/adler32.c ../lib/zlib/compress.c ../lib/zlib/crc32.c ../lib/zlib/gzio.c ../lib/zlib/uncompr.c \
             ../lib/zlib/deflate.c ../lib/zlib/trees.c ../lib/zlib/zutil.c ../lib/zlib/inflate.c ../lib/zlib/infback.c \
             ../lib/zlib/inftrees.c ../lib/zlib/inffast.c
