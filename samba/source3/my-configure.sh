#!/bin/bash
#CROSS_COMPILE=/usr/bin/arm-linux-gnueabi-
#CROSS_COMPILE=/home/eddie/CodeSourcery/Sourcery_G++_Lite/bin/arm-none-eabi-
#CROSS_COMPILE=/opt/FriendlyARM/toolschain/4.4.3/bin/arm-none-linux-gnueabi-
source my-common.sh

./configure \
CC="$CROSS_COMPILE"gcc \
AR="$CROSS_COMPILE"ar \
LD="$CROSS_COMPILE"ld \
RANLIB="$CROSS_COMPILE"ranlib \
CFLAGS="--sysroot=$SYSROOT -DNO_pw_gecos -DNO_statvfs -DFIX_ANDROID_DNS" \
--host=i686 \
--target=arm-linux \
--disable-cups \
--disable-iprint \
--prefix=$RUNTIME_DIR \
--exec-prefix=$RUNTIME_DIR \
--with-logfilebase=$RUNTIME_DIR/var/log \
--with-swatdir=$RUNTIME_DIR/usr/local/swat \
--with-rootsbindir=$RUNTIME_DIR/sbin \
--with-lockdir=$RUNTIME_DIR/var/lock \
--with-piddir=$RUNTIME_DIR/var/lock \
--with-privatedir=$RUNTIME_DIR \
--with-configdir=$CONFIG_DIR \
--cache-file=armsel-linux.cache \
--with-static-modules=vfs_fake_perms \
samba_cv_CC_NEGATIVE_ENUM_VALUES=yes \
