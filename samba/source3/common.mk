LOCAL_CFLAGS += -D_SAMBA_BUILD_=3
LOCAL_CFLAGS += -DHAVE_CONFIG_H 
	
LOCAL_CFLAGS += $(PATH_FLAGS)
LOCAL_CFLAGS += -DNO_pw_gecos
LOCAL_CFLAGS += -DNO_statvfs
LOCAL_CFLAGS += -DFIX_ANDROID_DNS

LOCAL_C_INCLUDES := $(LOCAL_PATH) $(LOCAL_PATH)/include $(LOCAL_PATH)/../lib/replace $(LOCAL_PATH)/../lib/tevent $(LOCAL_PATH)/libaddns $(LOCAL_PATH)/librpc $(LOCAL_PATH)/../lib/talloc $(LOCAL_PATH)/../lib/tdb/include \
	$(LOCAL_PATH)/../lib/popt $(LOCAL_PATH)/iniparser/src \
	$(LOCAL_PATH)/lib \
	$(LOCAL_PATH)/source4 \
	$(LOCAL_PATH)/.. \
	$(LOCAL_PATH)/../lib/zlib


LOCAL_LDFLAGS := -pie -Wl,-z,relro -Wl,--as-needed -ldl

#LOCAL_STATIC_LIBRARIES := libzipfile