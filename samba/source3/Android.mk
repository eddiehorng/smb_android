LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

prefix=/cache/samba
exec_prefix=/cache/samba
sysconfdir=${prefix}/etc
localstatedir=${prefix}/var
datarootdir=${prefix}/share

BASEDIR= /cache/samba
BINDIR = ${exec_prefix}/bin
# sbindir is mapped to bindir when compiling SAMBA in 2.0.x compatibility mode.
SBINDIR = ${exec_prefix}/sbin
ROOTSBINDIR = /cache/samba/sbin
LIBDIR = ${exec_prefix}/lib
MODULESDIR = ${exec_prefix}/lib
INCLUDEDIR=${prefix}/include
PAMMODULESDIR = ${exec_prefix}/lib/security
VFSLIBDIR = $(MODULESDIR)/vfs
PERFCOUNTLIBDIR = $(MODULESDIR)/perfcount
PDBLIBDIR = $(MODULESDIR)/pdb
RPCLIBDIR = $(MODULESDIR)/rpc
IDMAPLIBDIR = $(MODULESDIR)/idmap
NSSINFOLIBDIR = $(MODULESDIR)/nss_info
CHARSETLIBDIR = $(MODULESDIR)/charset
AUTHLIBDIR = $(MODULESDIR)/auth
CONFIGLIBDIR = $(MODULESDIR)/config
GPEXTLIBDIR = $(MODULESDIR)/gpext
CONFIGDIR = /system/etc/samba
VARDIR = ${prefix}/var
MANDIR = ${datarootdir}/man
DATADIR = ${datarootdir}

LOGFILEBASE = /cache/samba/var/log
CONFIGFILE = $(CONFIGDIR)/smb.conf
LMHOSTSFILE = $(CONFIGDIR)/lmhosts
NCALRPCDIR = ${VARDIR}/ncalrpc

# This is where smbpasswd et al go
PRIVATEDIR = /cache/samba

SMB_PASSWD_FILE = $(PRIVATEDIR)/smbpasswd
PRIVATE_DIR = $(PRIVATEDIR)

# This is where SWAT images and help files go
SWATDIR = /cache/samba/usr/local/swat

# This is where locale(mo) files go
LOCALEDIR= ${prefix}/share/locale

# the directory where lock files go
LOCKDIR = /cache/samba/var/lock

# FHS directories; equal to LOCKDIR if not using --with-fhs, but also settable
CACHEDIR = ${LOCKDIR}
STATEDIR = ${LOCKDIR}

# Where to look for (and install) codepage databases.
CODEPAGEDIR = ${MODULESDIR}

# the directory where pid files go
PIDDIR = /cache/samba/var/lock


PATH_FLAGS = -DSMB_PASSWD_FILE=\"$(SMB_PASSWD_FILE)\" \
	-DPRIVATE_DIR=\"$(PRIVATE_DIR)\" \
	-DCONFIGFILE=\"$(CONFIGFILE)\" \
	-DSBINDIR=\"$(SBINDIR)\" \
	-DBINDIR=\"$(BINDIR)\" \
	-DLMHOSTSFILE=\"$(LMHOSTSFILE)\" \
	-DSWATDIR=\"$(SWATDIR)\" \
	-DLOCKDIR=\"$(LOCKDIR)\" \
	-DPIDDIR=\"$(PIDDIR)\" \
	-DLIBDIR=\"$(LIBDIR)\" \
	-DMODULESDIR=\"$(MODULESDIR)\" \
	-DLOGFILEBASE=\"$(LOGFILEBASE)\" \
	-DSHLIBEXT=\"shared_libraries_disabled\" \
	-DNCALRPCDIR=\"$(NCALRPCDIR)\" \
	-DCONFIGDIR=\"$(CONFIGDIR)\" \
	-DCODEPAGEDIR=\"$(CODEPAGEDIR)\" \
	-DCACHEDIR=\"$(CACHEDIR)\" \
	-DSTATEDIR=\"$(STATEDIR)\" \
	-DLOCALEDIR=\"$(LOCALEDIR)\"

##########################################################################################################################
# smbd
include $(LOCAL_PATH)/common.mk

include $(LOCAL_PATH)/sources.mk

LOCAL_SRC_FILES := $(SMBD_OBJ) $(LIBTALLOC_OBJ) $(LIBTDB_OBJ) $(LIBWBCLIENT_OBJ) $(POPT_OBJ) $(ZLIB_OBJS)

LOCAL_MODULE:= smbdaemon

include $(BUILD_EXECUTABLE)

##########################################################################################################################
# smbpasswd
include $(CLEAR_VARS)

include $(LOCAL_PATH)/common.mk

LOCAL_SRC_FILES := $(SMBPASSWD_OBJ) $(LIBTALLOC_OBJ) $(LIBTDB_OBJ) $(LIBWBCLIENT_OBJ) $(POPT_OBJ) $(ZLIB_OBJS)

LOCAL_MODULE:= smbpasswd

include $(BUILD_EXECUTABLE)
