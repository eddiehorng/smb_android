#/bin/bash

adb remount
adb push bin/smbd /system/bin/

adb shell "mkdir -p /system/etc/samba"
adb shell "mkdir -p /cache/samba/var/log"
adb shell "mkdir -p /cache/samba/var/lock"
#adb shell "mkdir -p /cache/samba/lib"
#adb shell "mkdir -p /cache/samba/etc/samba"
adb shell "mkdir -p /cache/samba/var/tmp"

adb shell "export TMPDIR=/cache/samba/var/tmp/"

adb push smb.conf /system/etc/samba/
adb push passdb.tdb /system/etc/samba/
adb push secrets.tdb /system/etc/samba/