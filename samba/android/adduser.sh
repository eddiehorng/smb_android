user="root"
passwd="root"
flag=/cache/samba/user

if [ ! -f $flag ]; then
    echo -ne "$passwd\n$passwd\n" | smbpasswd -a -s $user
    touch $flag
fi
